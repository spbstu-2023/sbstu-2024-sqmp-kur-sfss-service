package main

import (
	"os"

	"gitlab.com/spbstu-2023-2025/spbstu-2024-sqmp-kup-sfss-service/cmd/sfss/internal/app/sfss"
	"gitlab.com/spbstu-2023-2025/spbstu-2024-sqmp-kup-sfss-service/cmd/sfss/internal/config"
)

func main() {
	// Open config file.
	configFile, err := os.Open(*ConfigPathFlag)
	if err != nil {
		panic(err)
	}

	// Get app config from config file.
	cfg, err := config.Unmarshal(configFile)
	if err != nil {
		panic(err)
	}

	// Create new main app.
	app := sfss.NewApp(cfg)

	// Run and defer close main app.
	app.Run()
	defer app.Close()
}
