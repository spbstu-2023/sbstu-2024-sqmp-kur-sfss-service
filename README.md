# Static File Storage Service (SFSS)

## Configuration

### YAML variables
Use `--config` flag for configure service. Default config placed in `configs/default.yaml`

## Make
### Build 
For run build of go service use target `gomake`. Binary file saved as `bin/sfss`.
```bash
    make gobuild
```

### Lint
For run golangci-lint use target `golint`. Logs and traces saved as `logs/lint-trace.log`.
```bash
    make golint
```

### Test
For run tests use target `gotest`. Logs and traces saved as `logs/unit-test-error-trace.log`.
```bash
    make gotest
```