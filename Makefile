gen-dir:
	mkdir -p logs/

golint: gen-dir
	golangci-lint run --config .golangci.yml ./... >"logs/lint-trace.txt"

gotest: gen-dir
	go test --cover -v  ./... >"logs/unit-test-trace.txt" 2>"logs/unit-test-error-trace.txt"
	go tool cover -html docs/coverage.txt -o docs/coverage.html

gogen:
	go generate ./...

gobuild: 
	go build -o bin/sfss cmd/sfss/main.go cmd/sfss/cli.go 

godocs:
	go run github.com/swaggo/swag/cmd/swag init -g cmd/sfss/main.go